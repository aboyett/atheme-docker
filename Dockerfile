FROM debian:stable

RUN apt-get update && apt-get install -y atheme-services && apt-get clean

RUN mkdir -p /var/run/atheme && chown irc:irc /var/run/atheme

# config should be located at /etc/atheme/atheme.conf
VOLUME /etc/atheme
VOLUME /var/lib/atheme

CMD ["/usr/bin/atheme-services", "-n"]
